import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud: number = 5;
  searchResults: string[];

  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidadorParametrizable(this.minLongitud)  /* se indica el parametro del validador*/
      ])],
      url: ['']
    });
  }

  ngOnInit(): void {
    const elemNombre = <HTMLElement>document.getElementById('nombre');
    // genero un observable de eventos
    fromEvent(elemNombre, 'input')  // selector de eventos, 'input' es el evento cada vez que see toca una tecla
      // pipe permite hacer operaciones en serie una tras otra
      .pipe(
        // devuelve el valor del campo luego de que la tecla fue presionada
        map((e: Event) => (e.target as HTMLInputElement).value), 
        // llega el value a text y avanza si se cumple la condicion
        filter(text => text.length > 2),
        // espera 200ms para dar tiempo durante el tipeo del usuario
        debounceTime(200),
        // avanza hasta que llega una cadena distinta a la anterior
        distinctUntilChanged(),
        // simulamos que consulamos un webservice
        switchMap(() => ajax('/assets/datos.json'))
      // nos suscribimos al cambio en la respuesta del ajax
      ).subscribe(AjaxResponse => {
          // asingamos la respuesta del servicio
          this.searchResults = AjaxResponse.response; 
      });
  }

  guardar(nombre:string, url:string):boolean{
    const d = new DestinoViaje(nombre, url);
    console.log(d);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): {[s:string]: boolean} | null{
    const l = control.value.toString().trim().length;
    if(l > 0 && l < 3){
      return {invalidNombre: true};
    }
    return null;
  }

  nombreValidadorParametrizable(minLong: number): ValidatorFn {
    // en el video dice usar (control: FormControl) pero no funciona, hay que usar (control: AbstractControl):
    return (control: AbstractControl): {[s:string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      if(l > 0 && l < minLong){
        return {minLongNombre: true};
      }
      return null;
    }
  }
}
