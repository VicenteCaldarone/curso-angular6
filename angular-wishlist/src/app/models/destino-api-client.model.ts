import { BehaviorSubject, Subject } from "rxjs";
import { DestinoViaje } from "./destino-viaje.model";

export class DestinosApiClient{
    destinos: DestinoViaje[];
    // uso "any" ya que sino me da error de compilacion al pasar null como argumento.
    current: Subject<DestinoViaje> = new BehaviorSubject<any>(null); 
    constructor(){
        this.destinos = []; // inicializamos vacio
    }

    public add(d: DestinoViaje){
        this.destinos.push(d);
    }

    public getAll():DestinoViaje[]{
        return this.destinos;
    }

    public getById(id: string | null): DestinoViaje {
        return this.destinos.filter(function(d){ return d.id.toString() === id })[0];
    }

    public elegir(d:DestinoViaje){
        this.destinos.forEach( x => x.setSelected(false));
        d.setSelected(true);
        this.current.next(d); // le seteamos el proximo valor al observable
    }

    public subscribeOnChange(fn: any){  // puse any porque sino me da error de compilacion
        this.current.subscribe(fn);
    }
}