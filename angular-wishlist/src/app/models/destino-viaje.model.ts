import {UUID} from 'uuid-generator-ts';

export class DestinoViaje{
    private selected:boolean;
    public servicios:string[];
    id = new UUID().toString();

    constructor(public nombre:string, public imagenUrl:string){
        this.servicios = ['pileta', 'desayuno'];
    }

    isSelected(){
        return this.selected;
    }

    setSelected(s:boolean){
        this.selected = s;
    }
}