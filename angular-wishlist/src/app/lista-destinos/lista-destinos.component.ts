import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinosApiClient } from '../models/destino-api-client.model';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];

  constructor(public destinosApiClient: DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];

    this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
      if(d != null){
        this.updates.push("Se ha elegido " + d.nombre);
      }
    });
    
  }

  ngOnInit(): void {
  }

  agregado(d:DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e:DestinoViaje){
      this.destinosApiClient.elegir(e); // le pasamos la responsabilidad al api client
  }
}
