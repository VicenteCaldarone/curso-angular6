import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() posicion: number;
  @HostBinding('attr.class') cssClass = 'col-md-4'; 
  /* probando pude verificar que en vez de HostBinding se pude usar tambien esto: 
    <app-destino-viaje *ngFor="let d of destinos" [destino]="d" class="col-md-4"></app-destino-viaje>
  */
  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor() { 
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino);
    return false; // para que no nos genere ningun efecto en el html
  }
}
