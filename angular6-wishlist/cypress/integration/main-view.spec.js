describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular6-wishlist');
        cy.get('h1 b').should('contain', 'HOLA es');
    });
    it('busca por Barcelona y lo encuentra', () => {
        cy.get('#nombre').type('Barcelona');
        cy.get('.form-group ul').should('contain', 'Barcelona');
    });    
    it('presiono en Guardar y carga la tarjeta', () => {
        cy.get('.btn-primary').click();
        cy.get('.card-desc h3').should('contain', 'Barcelona');
    });
});