import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinosApiClient } from '../../models/destino-api-client.model';
import { DestinoViaje } from '../../models/destino-viaje.model';
 
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    /* nos vamos a suscribir a solo una parte del estado
      nos importan solo las actializaciones sobre destinos, en particular sobre el atributo "favorito"
    */
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {  // en data llega el valor de lo que ha cambiado
        const fav = data;
        if(data != null){
          this.updates.push("Se ha elegido " + data.nombre);
        }
      });
      /* asignarle los cambios cada vez que cambia items dentro 
        del feature destinos en el state de nuestro store
      */
      this.store.select(state => state.destinos.items)
      .subscribe(items => {
          this.all = items;
      });
  }
 
  ngOnInit(): void {
  }
 
  agregado(d:DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }
 
  elegido(e:DestinoViaje){
      this.destinosApiClient.elegir(e); // le pasamos la responsabilidad al api client
  }
}