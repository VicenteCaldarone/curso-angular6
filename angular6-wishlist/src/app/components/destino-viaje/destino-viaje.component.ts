import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { VoteDownAction, VoteUpAction } from '../../models/destinos-viajes-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations'; // operadores a usar

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      // estados de la animacion
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'  // estilo a aplicar
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'     // estilo a aplicar
      })),
      // transiciones entre los estados de la animación
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')   //  duración de la transición
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')   //  duración de la transición
      ]),
    ])
  ]  
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() posicion: number;
  @HostBinding('attr.class') cssClass = 'col-md-4'; 
  /* probando pude verificar que en vez de HostBinding se pude usar tambien esto: 
    <app-destino-viaje *ngFor="let d of destinos" [destino]="d" class="col-md-4"></app-destino-viaje>
  */
  @Output() clicked: EventEmitter<DestinoViaje>;
 
  constructor(private store: Store<AppState>) { 
    this.clicked = new EventEmitter();
  }
 
  ngOnInit(): void {
  }
 
  ir(){
    this.clicked.emit(this.destino);
    return false; // para que no nos genere ningun efecto en el html
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
