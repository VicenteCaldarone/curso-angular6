import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLInputElement;

  /**
   * podemos recibir por inyección de dependencias elementos sobre el cual estamos inyectando esta directiva
   * @param elRef 
   */
  constructor(private elRef: ElementRef) {
    this.element = elRef.nativeElement; // guardo la referencia
    // me suscribo al click de ese elemento
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento));
  }

  track(evento: Event): void {
    // buscamos dentro del elemento si tiene un atributo 'data-trackear-tags'
    const elemTags = this.element.attributes.getNamedItem('data-trackear-tags').value.split(' ');
    console.log(`||||||||||| track evento: "${elemTags}"`);
  }
}
