import { UUID } from "uuid-generator-ts";

export class DestinoViaje{
    private selected:boolean;
    public servicios:string[];
    id = new UUID().toString();
    
    constructor(public nombre:string, public imagenUrl:string, public votes: number = 0){
        this.servicios = ['pileta', 'desayuno'];
    }
 
    isSelected(){
        return this.selected;
    }
 
    setSelected(s:boolean){
        this.selected = s;
    }

    voteUp(){
        this.votes++;
    }

    voteDown(){
        this.votes--;
    }
}