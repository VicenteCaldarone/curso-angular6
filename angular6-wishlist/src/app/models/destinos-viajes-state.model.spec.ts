import {
    reducerDestinosViajes,
    DestinosViajesState,
    initializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction,
    ElegidoFavoritoAction,
    VoteUpAction,
    VoteDownAction
} from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        // Setup: armamos los objetos que necesitamos para testear
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        // Actions: acciones sobre el modelo (los hacemos interactuar entre si los objetos)
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        // Asseert: validamos el resultado esperado
        expect(newState.items.length).toEqual(2);   // que la cantidad de destinos sea 2.
        expect(newState.items[0].nombre).toEqual('destino 1');  // que el primer destino sea 'destino 1'.
        // Tear Donw: borrar los datos que se hayan generado, por ej. en una Base de Datos.
        // *** codigo de Tear Down. ***
    });

    it('should reduce new item added', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });

    it('should reduce favorite', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const destinoViaje = new DestinoViaje('Madrid', 'urlMadrid');
        const actionPrev: NuevoDestinoAction = new NuevoDestinoAction(destinoViaje);
        const prevStateFav: DestinosViajesState = reducerDestinosViajes(prevState, actionPrev);

        const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(destinoViaje);
        const newState: DestinosViajesState = reducerDestinosViajes(prevStateFav, action);
        expect(newState.items[0].isSelected()).toEqual(true);
    });

    it('should reduce vote up', () => {
        const initState: DestinosViajesState = initializeDestinosViajesState();
        const destinoViaje1 = new DestinoViaje('Madrid', 'urlMadrid');
        const destinoViaje2 = new DestinoViaje('New York', 'urlNewYork');
        const action1: NuevoDestinoAction = new NuevoDestinoAction(destinoViaje1);
        const prevState1: DestinosViajesState = reducerDestinosViajes(initState, action1);

        const action2: NuevoDestinoAction = new NuevoDestinoAction(destinoViaje2);
        const prevState2: DestinosViajesState = reducerDestinosViajes(prevState1, action2);

        const action: VoteUpAction = new VoteUpAction(destinoViaje2);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState2, action);
        expect(newState.items[0].votes).toEqual(0);
        expect(newState.items[1].votes).toEqual(1);
    });

    it('should reduce vote down', () => {
        const initState: DestinosViajesState = initializeDestinosViajesState();
        const destinoViaje1 = new DestinoViaje('Madrid', 'urlMadrid');
        const destinoViaje2 = new DestinoViaje('New York', 'urlNewYork');
        const action1: NuevoDestinoAction = new NuevoDestinoAction(destinoViaje1);
        const prevState1: DestinosViajesState = reducerDestinosViajes(initState, action1);

        const action2: NuevoDestinoAction = new NuevoDestinoAction(destinoViaje2);
        const prevState2: DestinosViajesState = reducerDestinosViajes(prevState1, action2);

        const action3: VoteUpAction = new VoteUpAction(destinoViaje2);
        const prevState3: DestinosViajesState = reducerDestinosViajes(prevState2, action3);

        const action: VoteDownAction = new VoteDownAction(destinoViaje2);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState3, action);

        expect(newState.items[0].votes).toEqual(0);
        expect(newState.items[1].votes).toEqual(0);
    });
    
});